resource "aws_launch_configuration" "this" {
  lifecycle {
    create_before_destroy = true
  }

  name_prefix                 = "${var.name}-"
  image_id                    = "${var.ami}"
  iam_instance_profile        = "${module.iam.instance_profile["arn"]}"
  instance_type               = "${var.instance_type}"
  associate_public_ip_address = "${var.associate_public_ip_address}"
  ebs_optimized               = "${var.ebs_optimized}"
  key_name                    = "${var.key_pair}"
  user_data                   = "${var.user_data}"

  security_groups = ["${
    concat(
      list(module.instance_security_group.security_group["id"]),
      var.extra_security_groups
    )
  }"]

  root_block_device {
    volume_type           = "${var.root_volume_type}"
    volume_size           = "${var.root_volume_size}"
    iops                  = "${var.root_volume_iops}"
    delete_on_termination = "${var.root_volume_delete_on_termination}"
  }

  ephemeral_block_device = "${var.ephemeral_volumes}"
}

output "launch_configuration" {
  value = {
    id   = "${aws_launch_configuration.this.id}"
    name = "${aws_launch_configuration.this.name}"
  }
}
