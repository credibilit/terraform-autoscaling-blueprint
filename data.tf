data "aws_subnet" "this" {
  id = "${var.subnets[0]}"
}

locals {
  name_tag = {
    key = "Name",
    value = "${var.name}",
    propagate_at_launch = true
  }
}
