module "iam" {
  source          = "git::https://bitbucket.org/credibilit/terraform-ec2-role-blueprint.git?ref=1.0.2"

  name            = "${var.name}"
  description     = "EC2 policy for ASG ${var.name} instances"
  path            = "${var.iam_path}"
  policies_arn    = "${var.iam_policies_arn}"
  policies_count  = "${var.iam_policies_count}"
}

output "iam_instance_profile" {
  value = "${module.iam.instance_profile}"
}

output "iam_role" {
  value = "${module.iam.role}"
}
