resource "aws_iam_policy" "sample_policy" {
  lifecycle {
    create_before_destroy = true
  }

  name_prefix = "sample-policy-${local.name}-"
  policy      = "${data.aws_iam_policy_document.sample_policy.json}"
}
