// Account ID for provider
variable account {}

// Static values
locals {
  name                      = "asg-acme-test"
  vpc_cidr                  = "10.0.0.0/16"
  az_count                  = 2
  public_subnets_cidr_block = [
    "10.0.0.0/24",
    "10.0.1.0/24"
  ]
  tags = {
    Environment = "test"
    Stack = "ASG blueprint"
  }
  asg_tags = [
    {
      key = "Environment"
      value = "test"
      propagate_at_launch = true
    },
    {
      key = "Stack"
      value = "ASG blueprint"
      propagate_at_launch = true
    }
  ]
}

data "aws_availability_zones" "azs" { }

data "aws_iam_policy_document" "sample_policy" {
  statement {
    sid = "DescribeEC2"
    effect = "Allow"
    actions = ["ec2:Describe*"]
    resources = ["*"]
  }
}

data "aws_ami" "ubuntu" {
  most_recent = true
  owners = ["099720109477"]

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-*-16.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}
