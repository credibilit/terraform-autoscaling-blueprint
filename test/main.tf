module "asg" {
  source                = "../"

  name                        = "${local.name}"
  iam_policies_arn            = ["${aws_iam_policy.sample_policy.arn}"]
  iam_policies_count          = 1
  subnets                     = "${module.network.public_subnets}"
  ami                         = "${data.aws_ami.ubuntu.id}"
  key_pair                    = "${aws_key_pair.kp.id}"
  extra_security_groups       = ["${module.ssh.security_group["id"]}"]
  associate_public_ip_address = true
  tags                        = "${local.tags}"
  asg_tags                    = "${local.asg_tags}"
}

output "iam_instance_profile" {
  value = "${module.asg.iam_instance_profile}"
}

output "iam_role" {
  value = "${module.asg.iam_role}"
}

output "default_security_group" {
  value = "${module.asg.default_security_group}"
}

output "launch_configuration" {
  value = "${module.asg.launch_configuration}"
}

output "autoscaling_group" {
  value = "${module.asg.autoscaling_group}"
}

output "cloudwatch_alarms" {
  value = "${module.asg.cloudwatch_alarms}"
}

output "cpu_scaling_out" {
  value = "${module.asg.cpu_scaling_out}"
}

output "cpu_scaling_in" {
  value = "${module.asg.cpu_scaling_in}"
}
