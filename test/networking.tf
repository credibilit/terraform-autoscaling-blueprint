resource "aws_key_pair" "kp" {
  lifecycle {
    create_before_destroy = true
  }

  key_name_prefix = "${local.name}"
  public_key = "${file("~/.ssh/id_rsa.pub")}"
}

module "network" {
  source                    = "git::ssh://git@bitbucket.org/credibilit/terraform-vpc-blueprint.git?ref=1.1.0"
  name                      = "${local.name}"
  domain_name               = "${local.name}.local"
  cidr_block                = "${local.vpc_cidr}"
  azs                       = "${data.aws_availability_zones.azs.names}"
  az_count                  = "${local.az_count}"
  hosted_zone_comment       = "An internal hosted zone for testing"
  public_subnets_cidr_block = "${local.public_subnets_cidr_block}"
}

module "ssh" {
  source                          = "git::ssh://git@bitbucket.org/credibilit/terraform-aws-security-group-blueprint.git?ref=0.0.3"
  name                            = "ssh-${local.name}"
  description                     = "Sample SSH security group"
  vpc                             = "${module.network.vpc["id"]}"
  ingress_from_cidr_blocks        = [
    {
      protocol    = "tcp"
      from_port   = 22
      to_port     = 22
      cidr_blocks = "0.0.0.0/0"
    }
  ]
  ingress_from_cidr_blocks_count  = 1
}
