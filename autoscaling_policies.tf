resource "aws_cloudwatch_metric_alarm" "this_high_cpu" {
  alarm_name          = "asg-${var.name}-high-cpu-utilization"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "${var.default_scaling_policy_high_cpu_evaluation_periods}"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "${var.default_scaling_policy_high_cpu_period}"
  statistic           = "Average"
  threshold           = "${var.default_scaling_policy_high_cpu_threshold}"
  dimensions {
    AutoScalingGroupName = "${aws_autoscaling_group.this.name}"
  }
  alarm_description = "This monitor EC2 high CPU utilization for ASG ${var.name}"
  alarm_actions     = ["${aws_autoscaling_policy.this_cpu_scaling_out.arn}"]

  count = "${var.enable_default_scaling_policies ? 1 : 0}"
}

resource "aws_cloudwatch_metric_alarm" "this_low_cpu" {
  alarm_name          = "asg-${var.name}-low-cpu-utilization"
  comparison_operator = "LessThanThreshold"
  evaluation_periods  = "${var.default_scaling_policy_low_cpu_evaluation_periods}"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "${var.default_scaling_policy_low_cpu_period}"
  statistic           = "Average"
  threshold           = "${var.default_scaling_policy_low_cpu_threshold}"
  dimensions {
    AutoScalingGroupName = "${aws_autoscaling_group.this.name}"
  }
  alarm_description = "This monitor EC2 low CPU utilization for ASG ${var.name}"
  alarm_actions     = ["${aws_autoscaling_policy.this_cpu_scaling_in.arn}"]

  count = "${var.enable_default_scaling_policies ? 1 : 0}"
}

output "cloudwatch_alarms" {
  value = {
    high_cpu_id = "${aws_cloudwatch_metric_alarm.this_high_cpu.*.id}"
    low_cpu_id = "${aws_cloudwatch_metric_alarm.this_low_cpu.*.id}"
  }
}

// Auto Scaling Policy
resource "aws_autoscaling_policy" "this_cpu_scaling_out" {
  name                   = "cpu-scaling-out"
  scaling_adjustment     = "${length(var.subnets)}"
  adjustment_type        = "ChangeInCapacity"
  cooldown               = "${var.default_scaling_policy_scaling_out_cooldown}"
  autoscaling_group_name = "${aws_autoscaling_group.this.name}"

  count = "${var.enable_default_scaling_policies ? 1 : 0}"
}

output "cpu_scaling_out" {
  value = {
    arn                     = "${aws_autoscaling_policy.this_cpu_scaling_out.*.arn}"
    name                    = "${aws_autoscaling_policy.this_cpu_scaling_out.*.name}"
    autoscaling_group_name  = "${aws_autoscaling_policy.this_cpu_scaling_out.*.autoscaling_group_name}"
    adjustment_type         = "${aws_autoscaling_policy.this_cpu_scaling_out.*.adjustment_type}"
    policy_type             = "${aws_autoscaling_policy.this_cpu_scaling_out.*.policy_type}"
  }
}

resource "aws_autoscaling_policy" "this_cpu_scaling_in" {
  name                   = "cpu-scaling-in"
  scaling_adjustment     = -1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = "${var.default_scaling_policy_scaling_in_cooldown}"
  autoscaling_group_name = "${aws_autoscaling_group.this.name}"

  count = "${var.enable_default_scaling_policies ? 1 : 0}"
}

output "cpu_scaling_in" {
  value = {
    arn                     = "${aws_autoscaling_policy.this_cpu_scaling_in.*.arn}"
    name                    = "${aws_autoscaling_policy.this_cpu_scaling_in.*.name}"
    autoscaling_group_name  = "${aws_autoscaling_policy.this_cpu_scaling_in.*.autoscaling_group_name}"
    adjustment_type         = "${aws_autoscaling_policy.this_cpu_scaling_in.*.adjustment_type}"
    policy_type             = "${aws_autoscaling_policy.this_cpu_scaling_in.*.policy_type}"
  }
}
