variable "name" {
  type        = "string"
  description = "A name for AWS resources"
}

variable "iam_path" {
  type        = "string"
  description = "The IAM path for the EC2 role and instance profile"
  default     = "/"
}

variable "iam_policies_arn" {
  type        = "list"
  description = "A list of Managed IAM Policies ARNs"
  default     = []
}

variable "iam_policies_count" {
  type        = "string"
  description = "How many policies was passed on iam_policies_arn input parameter"
  default     = 0
}

variable "ingress_from_security_group" {
  type        = "list"
  description = "A list of rules using a another security group as source"
  default     = []
}

variable "ingress_from_security_group_count" {
  type        = "string"
  description = "How many of rules using a another security group as source exists"
  default     = 0
}

variable "ingress_from_cidr_blocks" {
  type        = "list"
  description = "A list of rules using a CIDR IP range as source"
  default     = []
}

variable "ingress_from_cidr_blocks_count" {
  type        = "string"
  description = "How many of rules using a CIDR IP range as source exists"
  default     = 0
}

variable "subnets" {
  type        = "list"
  description = "A list of subnets ids to launch the instances"
}

variable "key_pair" {
  description = "A default SSH key pair name"
  default     = ""
}

variable "extra_security_groups" {
  type        = "list"
  description = "A list of extras security groups to associate with the EC2 beyond the default one create by this blueprint"
  default     = []
}

variable "ami" {
  type        = "string"
  description = "The AMI id to launch instances"
}

variable "associate_public_ip_address" {
  description = "A default value for associating public IP automaticatly for the instances created by the Launch Configurations"
  default     = "false"
}

variable "instance_type" {
  description = "A default instance type for the EC2 instances created by the Launch Configurations"
  default     = "t2.micro"
}

variable "user_data" {
  description = "A default user data content for the EC2 instances created by the Launch Configurations"
  default     = ""
}

variable "enable_monitoring" {
  description = "A default value to enable or not the detailed CloudWatch monitoring on instances created by the Launch Configurations"
  default     = "true"
}

variable "ebs_optimized" {
  description = "A default value to enable or not the EBS optimized feature on instances created by the Launch Configurations"
  default     = "false"
}

variable "root_volume_type" {
  description = "A default value to define the type of root volume on EC2 instances created by the Launch Configurations"
  default     = "gp2"
}

variable "root_volume_size" {
  description = "A default size in GB of the root volume on EC2 instances created by the Launch Configurations"
  default     = "30"
}

variable "root_volume_iops" {
  description = "The default PIOPS of the root volume on EC2 instances created by the Launch Configurations wgen the type is io1"
  default     = "0"
}

variable "root_volume_delete_on_termination" {
  description = "The default flag to determine if the root volume on EC2 instances created by the Launch Configurations must be delete when the instance is terminated"
  default     = "true"
}

variable "ephemeral_volumes" {
  type        = "list"
  description = "A list with the ephemeral layout"
  default     = []
}

variable "max_size" {
  description = "The maximum size of the auto scale group. Default: 0"
  default     = "0"
}

variable "min_size" {
  description = "The minimum size of the auto scale group. Default: 0"
  default     = "0"
}

variable "desired_capacity" {
  description = "The desired size of the auto scale group. Default: 'max_size'"
  default     = ""
}


variable "health_check_grace_period" {
  description = "Time (in seconds) after instance comes into service before checking health. Default: 120"
  default     = "120"
}

variable "health_check_type" {
  description = "Controls how health checking is done. EC2 or ELB. Default: EC2"
  default     = "EC2"
}

variable "load_balancers" {
  type        = "list"
  description = "A list of load balancer names to add to the autoscaling group names"
  default     = []
}

variable "vpc_zone_identifier" {
  type        = "list"
  description = "A list of subnet IDs to launch resources in"
  default     = []
}

variable "target_group_arns" {
  type        = "list"
  description = "A list of aws_alb_target_group ARNs, for use with Application Load Balancing"
  default     = []
}

variable "termination_policies" {
  type        = "list"
  description = "A list of policies to decide how the instances in the auto scale group should be terminated"
  default     = ["OldestLaunchConfiguration", "OldestInstance", "Default"]
}

variable "enabled_metrics" {
  type        = "list"
  description = "A list of metrics to collect"

  default = [
    "GroupMinSize",
    "GroupMaxSize",
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupPendingInstances",
    "GroupStandbyInstances",
    "GroupTerminatingInstances",
    "GroupTotalInstances",
  ]
}

variable "default_cooldown" {
  description = "Time between a scaling activity and the succeeding scaling activity"
  default     = "300"
}

variable "force_delete" {
  type        = "string"
  description = "Force ASG deletion without waiting for EC2 to terminate"
  default     = false
}

variable "suspended_processes" {
  type        = "list"
  description = "A list of Autoscaling process to be ignored"
  default     = []
}

variable "wait_for_capacity_timeout" {
  type        = "string"
  description = "A maximum duration that Terraform should wait for ASG instances to be healthy before timing out. Setting this to 0 causes Terraform to skip all Capacity Waiting behavior."
  default     = "10m"
}

variable "min_elb_capacity" {
  type        = "string"
  description = "Minimium health instances to appear healthy on ELB before continues"
  default     = 0
}

variable "wait_for_elb_capacity" {
  type        = "string"
  description = "Number of health instances to appear healthy on all ELB before continues"
  default     = 0
}

variable "protect_from_scale_in" {
  type        = "string"
  description = "Activate termination protection on instances to avoid scale in events"
  default     = false
}

variable "metrics_granularity" {
  type        = "string"
  description = "Number of minutes to collect metrics"
  default     = "1Minute"
}

variable "asg_tags" {
  type        = "list"
  description = "A list with tags for ASG. Do not include the Name tag here once it already defined internally"
  default     = []
}

variable "tags" {
  type        = "map"
  description = "A map for extra tags for non ASG resources. Do not include the Name tag here once it already defined internally"
  default     = {}
}

variable "enable_default_scaling_policies" {
  description = "Enable default scale-in and scale-out policies based on CPU Utilization"
  default     = true
}

variable "default_scaling_policy_high_cpu_evaluation_periods" {
  description = "The number of periods over which data is compared to the specified threshold"
  default     = 5
}

variable "default_scaling_policy_high_cpu_period" {
  description = "The period in seconds over which the specified statistic is applied"
  default     = 60
}

variable "default_scaling_policy_high_cpu_threshold" {
  description = "The value against which the specified statistic is compared"
  default     = 60
}

variable "default_scaling_policy_scaling_out_cooldown" {
  description = "The amount of time, in seconds, after a scaling activity completes and before the next scaling activity can start"
  default     = 300
}

variable "default_scaling_policy_low_cpu_evaluation_periods" {
  description = "The number of periods over which data is compared to the specified threshold"
  default     = 3
}

variable "default_scaling_policy_low_cpu_period" {
  description = "The period in seconds over which the specified statistic is applied"
  default     = 60
}

variable "default_scaling_policy_low_cpu_threshold" {
  default     = 900
  description = "The value against which the specified statistic is compared"
  default     = 20
}

variable "default_scaling_policy_scaling_in_cooldown" {
  description = "The amount of time, in seconds, after a scaling activity completes and before the next scaling activity can start"
  default     = 3000
}
