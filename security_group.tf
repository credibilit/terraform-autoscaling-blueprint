module "instance_security_group" {
  source                            = "git::https://git@bitbucket.org/credibilit/terraform-aws-security-group-blueprint.git?ref=0.0.3"
  name                              = "${var.name}"
  description                       = "Default security group for instances on ASG ${var.name}"
  vpc                               = "${data.aws_subnet.this.vpc_id}"
  ingress_from_cidr_blocks          = "${var.ingress_from_cidr_blocks}"
  ingress_from_cidr_blocks_count    = "${var.ingress_from_cidr_blocks_count}"
  ingress_from_security_group       = "${var.ingress_from_security_group}"
  ingress_from_security_group_count = "${var.ingress_from_security_group_count}"
  tags                              = "${var.tags}"
}

output "default_security_group" {
  value = "${module.instance_security_group.security_group}"
}
