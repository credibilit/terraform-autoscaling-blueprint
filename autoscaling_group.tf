resource "aws_autoscaling_group" "this" {
  name_prefix               = "${var.name}-"
  max_size                  = "${var.max_size}"
  min_size                  = "${var.min_size}"
  desired_capacity          = "${var.desired_capacity}"
  default_cooldown          = "${var.default_cooldown}"
  force_delete              = "${var.force_delete}"
  launch_configuration      = "${aws_launch_configuration.this.name}"
  health_check_grace_period = "${var.health_check_grace_period}"
  health_check_type         = "${var.health_check_type}"
  termination_policies      = "${var.termination_policies}"
  vpc_zone_identifier       = ["${var.subnets}"]
  load_balancers            = ["${var.load_balancers}"]
  target_group_arns         = ["${var.target_group_arns}"]
  enabled_metrics           = "${var.enabled_metrics}"
  metrics_granularity       = "1Minute"
  suspended_processes       = "${var.suspended_processes}"
  wait_for_capacity_timeout = "${var.wait_for_capacity_timeout}"
  min_elb_capacity          = "${var.min_elb_capacity}"
  wait_for_elb_capacity     = "${var.wait_for_elb_capacity}"
  protect_from_scale_in     = "${var.protect_from_scale_in}"
  tags                      = "${concat(
      list(local.name_tag),
      var.asg_tags
  )}"
}

output "autoscaling_group" {
  value = {
    id                        = "${aws_autoscaling_group.this.id}"
    arn                       = "${aws_autoscaling_group.this.arn}"
    availability_zones        = "${aws_autoscaling_group.this.availability_zones}"
    min_size                  = "${aws_autoscaling_group.this.min_size}"
    max_size                  = "${aws_autoscaling_group.this.max_size}"
    default_cooldown          = "${aws_autoscaling_group.this.default_cooldown}"
    name                      = "${aws_autoscaling_group.this.name}"
    health_check_grace_period = "${aws_autoscaling_group.this.health_check_grace_period}"
    health_check_type         = "${aws_autoscaling_group.this.health_check_type}"
    subnets                   = "${aws_autoscaling_group.this.vpc_zone_identifier}"
  }
}
